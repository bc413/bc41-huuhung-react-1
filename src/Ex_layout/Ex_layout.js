import React, { Component } from 'react'
import Header from './Header'
import Welcome from './Welcome'
import Item from './Item'
import Footer from './Footer'

export default class Ex_layout extends Component {
  render() {
    return (
      <div>
        <Header/>
        <div className="container p-0">
            <Welcome/>
            <div className="row">
              <Item/>
              <Item/>
              <Item/>
              <Item/>
              <Item/>
              <Item/>
            </div>
        </div>
        <Footer/>
      </div>
    )
  }
}
